<?php
  /**
  * @package calendario-dogourmet
  * @version 0.1
  */
  /*
  Plugin Name: calendario dogourmet
  Plugin URI: http://hacemosloquenosgusta.com
  Description: sistema de votacion  basado en historias
  Author: Jose Avila
  Version: 0.1
  Author URI: http://joseavila.tk
  */
  include_once "actions.php";
  
  add_action( 'init', 'historia', 0 );
  add_filter( 'template_include', 'archive_template' );
  add_filter('manage_historia_posts_columns', 'votes_colunm', 1, 1);
  add_action('wp_enqueue_scripts', 'plug_style');
  add_action('wp_enqueue_scripts', 'plug_scripts');
  add_action('init', 'collect_like');
  add_action('init', 'hasAlreadyVoted');
  add_action('wp_ajax_nopriv_post-like', 'post_like');
  add_action('wp_ajax_post-like', 'post_like');
  
?>

