<?php get_header(); ?>
  <section class="h-single">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <picture>
      <?php the_post_thumbnail( 'full' ); ?>
    </picture>
    <div class="h-holder">
      <h1 class="main-title">Ficha del participante</h1>
      <h1 class="title"><?php the_title(); ?></h1>
      <img src="<?php echo plugins_url(); ?>/calendario-dogourmet/images/logotema.png" alt="" class="logotema">
      <article>
        <section>
          <?php the_content(); ?>
        </section>
      
      </article>
      <p class="post-like">
        <a data-post_id="" href="#" onclick="fb_login(); ">
            <span class="qtip like" title="I like this article">
              <i class="fa fa-heart"></i>
            </span>
          </a>
          <span class="count">Votar</span>
      </p>
      <div class="share-hold">
        <button>compartir</button>
        <div class="s-holder">
          <a id="shareBtn" href="#"><i class="fa fa-facebook"></i></a>
          <a class="twitter-share-button"
            href="https://twitter.com/intent/tweet?text=Hello%20world"
            data-size="large"><i class="fa fa-twitter" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"></i></a>
          <a href="whatsapp://send" data-text="Vota por tu historia favorita" data-href=""><i class="fa fa-whatsapp"></i></a>
          <!-- <a href="whatsapp://send" data-text="Vota por tu historia favorita" data-href="" class="wa_btn wa_btn_s" style="display:none">Share</a> -->
        </div>
      </div>
      <a href="<?php echo get_home_url(); ?>/historia" class="back">Conocer a los demas participantes</a>
      <?php endwhile; else : ?>
        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
      <?php endif; ?>
    </div>
  </section>
<?php get_footer(); ?>

<script>

  jQuery(document).ready(function() {
  
    jQuery(".post-like a").click(function(){
      
         heart = jQuery(this);
         // Retrieve post ID from data attribute
        //  post_id = heart.data("post_id");
         post_id = '<?php echo $post -> ID; ?>';
         url = '<?php echo admin_url('admin-ajax.php')?>';
         nonce = '<?php echo wp_create_nonce('ajax-nonce')?>';
         // Ajax call
         jQuery.ajax({
             type: "post",
             url: url,
             data: "action=post-like&nonce="+nonce+"&post_like=&post_id="+post_id,
             success: function(count){
                 // If vote successful
                 if(count != "already")
                 {
                     heart.addClass("voted");
                     heart.siblings(".count").text(count);
                 }
             }
         });
          
         return false;
    });
  });

  // FB SDK
  window.fbAsyncInit = function() {
    FB.init({
        appId   : '1148889845254686',
        oauth   : true,
        status  : true, // check login status
        cookie  : true, // enable cookies to allow the server to access the session
        xfbml   : true // parse XFBML
    });

  };
  function fb_login(){
      FB.login(function(response) {

          if (response.authResponse) {
              console.log('Welcome!  Fetching your information.... ');
              //console.log(response); // dump complete info
              access_token = response.authResponse.accessToken; //get access token
              user_id = response.authResponse.userID; //get FB UID

              FB.api('/me', function(response) {
                  user_email = response.email; //get user email
            // you can store this data into your database             
              });
              
            } else {
              //user hit cancel button
              console.log('User cancelled login or did not fully authorize.');
              
            }
            console.log(response);
          }, {
            scope: 'publish_actions,email'
      });
  }
  (function() {
      var e = document.createElement('script');
      e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
      e.async = true;
      document.getElementById('fb-root').appendChild(e);
  }());

  // share 
  document.getElementById('shareBtn').onclick = function() {
  FB.ui({
    method: 'share',
    display: 'popup',
    href: 'https://ve.dogourmet.com/historia/',
  }, function(response){});
}

</script>