<?php get_header(); ?>
  <section class="h-archive">
    <div class="c-holder">
      <img class="logotema" src="<?php echo plugins_url(); ?>/calendario-dogourmet/images/logotema.png" alt="">
      <h1 class="main-title">Conoce y vota <br> <span>por tus votantes favoritos</span></h1>
      <div class="search-holder">
        <!-- <input type="text" name="" placeholder="buscar participante"> -->
        <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
      </div>
      <div class="p-holder">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <article>
          <a class="p-name" href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
          <figure>
            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'medium' ); ?></a>
          </figure>
        </article>
        <?php endwhile; else : ?>
        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php endif; ?>
        <?php page_nav(); ?>
      </div>

    </div>
  </section>
<?php get_footer(); ?>