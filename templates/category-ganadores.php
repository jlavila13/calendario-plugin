<?php get_header(); ?>
  <section class="h-ganadores">
    <div class="c-holder">
      <img class="logotema" src="<?php echo plugins_url(); ?>/calendario-dogourmet/images/logotema.png" alt="">
      <h1 class="main-title">Conoce a los ganadores <br><span>de calendario 2018</span></h1>
      <div class="p-holder flexslider">
        <ul class="slides">
        <?php 
          $args = array(
            'post_type' => 'historia',
            'cat' => 3,
            // 'cat' => 26,
            'post_per_page' => 12
          );
          $the_query = new WP_Query( $args );
        ?>
        <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <li>
          <article>
            <div class="polaroid">
              <div class="imagen">
                <img class="scribble" src="<?php echo plugins_url(); ?>/calendario-dogourmet/images/letter.png" alt="" draggable="false">
                <figure>
                  <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full' ); ?></a>
                </figure>
              </div>
              <div class="info-holder">
                  <h1>¡Protagonistas!<br>del calendario <img src="<?php echo plugins_url(); ?>/calendario-dogourmet/images/letter.png?>" alt="">2018</h1>
                  <h2><?php the_title(); ?></h2>
                <section>
                  <?php the_content();?>
                </section>
              </div>
            </div>
            <button class="button btnSave"><i class="fa fa-cloud-download"></i></button>
            <div href="#" class="button">
            <div class="share-hold">
              <i class="fa fa-share-alt"></i>
              <div class="s-holder">
                <a id="shareBtn" href="<?php the_permalink(); ?>"><i class="fa fa-facebook"></i></a>
                <a class="twitter-share-button"
                  href="https://twitter.com/intent/tweet?text=Hello%20world"
                  data-size="large"><i class="fa fa-twitter" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"></i></a>
                  <a href="whatsapp://send" data-text="Vota por tu historia favorita" data-href=""><i class="fa fa-whatsapp"></i></a>
              </div>
            </div>
            </div>
          </article>
        </li>
        <?php endwhile; else : ?>
        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php endif; ?>
        </ul>
      </div>
      <di id="img-out"></di>
    </div>
  </section>
  <script>
    //   flexslider
     jQuery(window).load(function() {
      jQuery('.flexslider').flexslider({
          animation: "slide",
          pauseOnHover: true,
        });
      });
      //  capturas la imagen
      jQuery(".btnSave").click(function() {
        var objeto = jQuery(this).siblings('.polaroid');
        html2canvas(objeto,{
            onrendered: function(canvas) {
                theCanvas = canvas;
                canvas.toBlob(function(blob) {
                saveAs(blob, "ficha.png");
                });
              }
        });
      });

      jQuery('.fa-share').click(function(){
        jQuery('.s-holder').toggleSlide();
        return false;
      });

  </script>
<?php get_footer(); ?>

