<?php
  // Register Custom Post Type
  function historia() {
  
    $labels = array(
      'name'                  => _x( 'historias', 'Post Type General Name', 'text_domain' ),
      'singular_name'         => _x( 'Post Type', 'Post Type Singular Name', 'text_domain' ),
      'menu_name'             => __( 'historias', 'text_domain' ),
      'name_admin_bar'        => __( 'Historias', 'text_domain' ),
      'archives'              => __( 'Item Archives', 'text_domain' ),
      'attributes'            => __( 'Item Attributes', 'text_domain' ),
      'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
      'all_items'             => __( 'All Items', 'text_domain' ),
      'add_new_item'          => __( 'Add New Item', 'text_domain' ),
      'add_new'               => __( 'Add New', 'text_domain' ),
      'new_item'              => __( 'New Item', 'text_domain' ),
      'edit_item'             => __( 'Edit Item', 'text_domain' ),
      'update_item'           => __( 'Update Item', 'text_domain' ),
      'view_item'             => __( 'View Item', 'text_domain' ),
      'view_items'            => __( 'View Items', 'text_domain' ),
      'search_items'          => __( 'Search Item', 'text_domain' ),
      'not_found'             => __( 'Not found', 'text_domain' ),
      'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
      'featured_image'        => __( 'Featured Image', 'text_domain' ),
      'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
      'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
      'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
      'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
      'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
      'items_list'            => __( 'Items list', 'text_domain' ),
      'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
      'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
      'label'                 => __( 'Post Type', 'text_domain' ),
      'description'           => __( 'Historias para concurso', 'text_domain' ),
      'labels'                => $labels,
      'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
      'taxonomies'            => array( 'category' ),
      'hierarchical'          => true,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 5,
      'menu_icon'             => '',
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => true,		
      'exclude_from_search'   => false,
      'publicly_queryable'    => true,
      'capability_type'       => 'page',
    );
    register_post_type( 'historia', $args );
  
  }
  
  if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
//   columns
  function votes_colunm($columns){
    unset($columns['date']);
    $columns['votos'] = __('Cant. de votos');
    return $columns;
  }
  add_filter('manage_vote_column', 'votes_column');
  add_filter('manage_edit_vote_sortable_column', 'votes_column');

  function votes_data($colname, $voteid){
      if($colname =='votos')
      echo get_post_meta($voteid, '_votes_count', true);
	}
	add_action('manage_historia_posts_custom_column', 'votes_data', 10, 2);
  

  //styles
  function fontawesome_dashboard() {
    wp_enqueue_style('fontawesome', 'http:////netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css', '', '4.0.3', 'all'); 
  }

  add_action('admin_init', 'fontawesome_dashboard');

  function fontawesome_icon_dashboard() {
  echo "<style type='text/css' media='screen'>
          menu-posts-historia:before, #adminmenu #menu-posts-historia div.wp-menu-image:before {
          font-family: Fontawesome !important;
          content: '\\f1b0';
      }
          </style>";
  }
  add_action('admin_head', 'fontawesome_icon_dashboard');

  function plug_style(){
    wp_register_style( 'historias', plugins_url( 'css/hist_style.css', __FILE__ ) );
    wp_register_style( 'flexslider', 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/flexslider.min.css' );
    wp_enqueue_style( 'historias' );
    wp_enqueue_style( 'flexslider' );
    
  }

  function plug_scripts(){
    wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js', '1.0', true );
    wp_register_script( 'icons', 'https://use.fontawesome.com/5ed2ab4a08.js', '1.0', true );
    wp_register_script( 'custom', plugins_url( 'scripts/hist-scripts.js', __FILE__ ), '1.0', true );
    wp_register_script( 'flexslider', 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/jquery.flexslider-min.js', '1.0', true );
    wp_register_script( 'htoc', 'https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js', '1.0', true );
    wp_register_script( 'base', 'https://cdn.jsdelivr.net/npm/file-saver@1.3.3/FileSaver.min.js', '1.0', true );
    wp_register_script( 'htoi', 'https://cdn.jsdelivr.net/npm/canvas2image@1.0.5/canvas2image.min.js', '1.0', true );
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'icons' );
    wp_enqueue_script( 'custom' );
    wp_enqueue_script( 'like_post' );
    wp_enqueue_script( 'flexslider' );
    wp_enqueue_script( 'htoc' );
    wp_enqueue_script( 'base' );
    wp_enqueue_script( 'htoi' );
    
  }

  function archive_template($template){ 
    $aver = is_post_type_archive('historia');
    echo $aver;
    if(is_post_type_archive('historia')){
      $template = WP_PLUGIN_DIR . '/' . plugin_basename( dirname(__FILE__)) . '/templates/archive-historia.php';
    }
    if( is_singular( 'historia' )){
        $template = WP_PLUGIN_DIR . '/' . plugin_basename( dirname(__FILE__)) . '/templates/single-historia.php';
    }
    if( is_category( 'ganadores' )){
        $template = WP_PLUGIN_DIR . '/' . plugin_basename( dirname(__FILE__)) . '/templates/category-ganadores.php';
    }
    return $template;
  }

  function collect_like(){
      wp_localize_script('plug_scripts', 'ajax_var', array(
          'url' => admin_url('admin-ajax.php'),
          'nonce' => wp_create_nonce('ajax-nonce')
      ));
  }

  function post_like(){
      // Check for nonce security
      $nonce = $_POST['nonce'];

    
      if ( !wp_verify_nonce( $nonce, 'ajax-nonce' ) )
          die ( 'Busted!');
       
      if(isset($_POST['post_like']))
      {
          // Retrieve user IP address
          $ip = $_SERVER['REMOTE_ADDR'];
        //   $ip = '127.0.0.1';
          $post_id = $_POST['post_id'];
           
          // Get voters'IPs for the current post
          $meta_IP = get_post_meta($post_id, "voted_IP");
          $voted_IP = $meta_IP[0];
   
          if(!is_array($voted_IP))
              $voted_IP = array();
           
          // Get votes count for the current post
          $meta_count = get_post_meta($post_id, "votes_count", true);
   
          // Use has already voted ?
          if(!hasAlreadyVoted($post_id)){
              $voted_IP[$ip] = time();
   
              // Save IP and increase votes count
              update_post_meta($post_id, "_voted_IP", $voted_IP);
              update_post_meta($post_id, "_votes_count", ++$meta_count);
               
              // Display count (ie jQuery return value)
              echo $meta_count;
          }
          else
              echo "gracias!";
      }
      exit; 
    }
// facebook open graph
function hasAlreadyVoted($post_id)
{
    global $timebeforerevote;
    
    // Retrieve post votes IPs
    $meta_IP = get_post_meta($post_id, "voted_IP");
    $voted_IP = $meta_IP[0];
        
    if(!is_array($voted_IP))
        $voted_IP = array();
            
    // Retrieve current user IP
    $ip = $_SERVER['REMOTE_ADDR'];
        
    // If user has already voted
    if(in_array($ip, array_keys($voted_IP)))
    {
        $time = $voted_IP[$ip];
        $now = time();
            
        // Compare between current time and vote time
        if(round(($now - $time) / 60) > $timebeforerevote)
            return false;
                
        return true;
    }
        
    return false;
}

//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
    return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
}
add_filter('language_attributes', 'add_opengraph_doctype');

//Lets add Open Graph Meta Info

function insert_fb_in_head() {
global $post;
if ( !is_singular()) //if it is not a post or a page
    return;
    echo '<meta property="fb:admins" content="1148889845254686"/>';
    echo '<meta property="og:title" content="' . get_the_title() . '"/>';
    echo '<meta property="og:type" content="article"/>';
    echo '<meta property="og:url" content="' . get_permalink() . '"/>';
    echo '<meta property="og:site_name" content="Historias Dogourmet"/>';
if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
    $default_image="<?php echo plugins_url(); ?>/calendario-dogourmet/images/fb-cover.png"; //replace this with a default image on your server or an image in your media library
    echo '<meta property="og:image" content="' . $default_image . '"/>';
}
else{
    $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
    echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
}
echo "
";
}
add_action( 'wp_head', 'insert_fb_in_head', 5 );

// insert twitter metas
function meta_tweet(){
    echo '<meta name="twitter:card" content="summary_large_image">';
    echo '<meta name="twitter:title" content="Vota por tu favorito" />';
    echo '<meta name="twitter:url" content="'. get_permalink() .'" />';
    echo '<meta name="twitter:desciption" content="Vota por tu peludito favorito" />';
    echo '<meta name="twitter:site" content="campamento-pan" />';
    echo '<meta name="twitter:image" content="" />';
    if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
        $default_image="<?php echo plugins_url(); ?>/calendario-dogourmet/images/fb-cover.png"; //replace this with a default image on your server or an image in your media library
        echo '<meta name="twitter:image" content="' . $default_image . '"/>';
    }
    else{
        $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
        echo '<meta name="twitter:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
    }
}
// paginacion
function page_nav() {
 
    if( is_singular() )
        return;
 
    global $wp_query;
 
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    echo '<div class="navigation"><ul>' . "\n";
 
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
 
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
 
        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";
 
        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
 
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );
 
    echo '</ul></div>' . "\n";
 
}
?>